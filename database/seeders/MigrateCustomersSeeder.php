<?php

namespace Database\Seeders;

use App\Models\Mappers\CustomerToUserDto;
use App\Models\Source\Customer;
use Illuminate\Database\Seeder;

class MigrateCustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::query()
            ->whereNotNull('Phone')->where('Phone', '<>', '')
            ->with('address')->orderBy('id')
            ->chunk(100, function ($chunk) {
                CustomerToUserDto::make($chunk)->insertToDb();
            });
    }
}
