<?php

namespace Database\Seeders;

use App\Models\Mappers\CleanerToUserDto;
use App\Models\Source\Cleaner;
use Illuminate\Database\Seeder;

class MigrateCleanersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cleaner::query()
            ->whereNotNull('Phone')->where('Phone', '<>', '')
            ->with(['locations', 'availability'])->orderBy('id')
            ->chunk(100, function ($chunk) {
                CleanerToUserDto::make($chunk)->insertToDb();
            });

    }
}
