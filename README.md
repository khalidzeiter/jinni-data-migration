## Configuration
- Change SOURCE_DB_DATABASE key in .env to source database name.
- Change DIST_DB_DATABASE key in .env to destination database name.

## Migrate Customers Data 

```shell
php artisan db:seed --class=MigrateCustomersSeeder
```

## Migrate Cleaners w/ coverage & roles data migrations 

```shell
php artisan db:seed --class=MigrateCleanersSeeder
```
