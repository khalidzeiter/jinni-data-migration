<?php

namespace App\Models\Source;

use Illuminate\Database\Eloquent\Model;

class CleanersLocation extends Model
{
    protected $table = 'CleanersLocations';

    protected $connection = 'data_migration.source';

    public function location()
    {
        return $this->belongsTo(Location::class, 'LocationID', 'ID');
    }
}
