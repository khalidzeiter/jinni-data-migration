<?php

namespace App\Models\Source;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'Districts';

    protected $connection = 'data_migration.source';
}
