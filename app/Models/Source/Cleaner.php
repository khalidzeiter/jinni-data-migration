<?php

namespace App\Models\Source;

use Illuminate\Database\Eloquent\Model;

class Cleaner extends Model
{
    protected $table = 'Cleaners';

    protected $connection = 'data_migration.source';

    public function locations()
    {
        return $this->hasMany(CleanersLocation::class, 'CleanerID', 'ID');
    }

    public function availability()
    {
        return $this->hasOne(CleanersAvailability::class, 'CleanerID', 'ID');
    }
}
