<?php

namespace App\Models\Source;

use Illuminate\Database\Eloquent\Model;

class CleanersAvailability extends Model
{
    protected $table = 'CleanersAvaliability';

    protected $connection = 'data_migration.source';

    protected $casts = [
        'Avaliability' => 'array'
    ];
}
