<?php

namespace App\Models\Source;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'Locations';

    protected $connection = 'data_migration.source';

    public function district()
    {
        return $this->belongsTo(District::class, 'DistrictID', 'ID');
    }
}
