<?php

namespace App\Models\Source;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'Customers';

    protected $connection = 'data_migration.source';

    public function address()
    {
        return $this->hasMany(CustomerAddress::class, 'UserID', 'ID');
    }
}
