<?php

namespace App\Models\Source;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $table = 'CustomerAddressBooks';

    protected $connection = 'data_migration.source';

    public function location()
    {
        return $this->belongsTo(Location::class, 'LocationID', 'ID');
    }
}
