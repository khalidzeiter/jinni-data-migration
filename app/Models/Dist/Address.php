<?php

namespace App\Models\Dist;

use Eloquent as Model;

/**
 * Class Address
 * @package App\Models
 * @version January 13, 2021, 8:02 pm UTC
 *
 * @property User user
 * @property integer id
 * @property string description
 * @property string address
 * @property double latitude
 * @property double longitude
 * @property boolean default
 * @property integer user_id
 */
class Address extends Model
{
    public $table = 'addresses';

    protected $connection = 'data_migration.dist';

    public $fillable = [
        'description',
        'address',
        'latitude',
        'longitude',
        'default',
        'space',
        'building_number',
        'floor_number',
        'apartment_number',
        'user_id',
        'area_id',
        'street',
        'landmark',
        'building_type_id',
    ];

    protected $casts = [
        'description'      => 'string',
        'address'          => 'string',
        'latitude'         => 'double',
        'longitude'        => 'double',
        'default'          => 'boolean',
        'space'            => 'double',
        'building_number'  => 'string',
        'floor_number'     => 'integer',
        'apartment_number' => 'string',
        'user_id'          => 'integer'
    ];

    public static function getCurrentAreaId($oldDistrictId)
    {
        return [
                '13' => '63',
                '14' => '71',
                '15' => null,
                '16' => '78',
                '17' => '82',
                '18' => '27',
                '19' => '43',
                '20' => '46',
                '21' => '14',
                '22' => '18',
                '25' => '81',
                '26' => '79',
                '27' => '53',
                '28' => null,
                '29' => '35',
                '30' => null,
                '32' => '22',
                '33' => '20',
                '34' => null,
                '35' => '47',
                '36' => null,
                '37' => '8',
                '38' => '49',
            ][strval($oldDistrictId)] ?? null;
    }

}
