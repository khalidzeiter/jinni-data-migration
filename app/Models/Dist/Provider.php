<?php

namespace App\Models\Dist;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    public $table = 'e_providers';

    protected $connection = 'data_migration.dist';
}
