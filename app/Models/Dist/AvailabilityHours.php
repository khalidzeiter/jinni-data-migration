<?php

namespace App\Models\Dist;

use Illuminate\Database\Eloquent\Model;

class AvailabilityHours extends Model
{
    public $table = 'availability_hours';

    protected $connection = 'data_migration.dist';
}
