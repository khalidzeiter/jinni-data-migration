<?php

namespace App\Models\Dist;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;

    public $table = 'users';

    protected $connection = 'data_migration.dist';

    public $fillable = [
        'name',
        'email',
        'gender',
        'national_id',
        'phone_number',
        'phone_verified_at',
        'password',
        'api_token',
        'device_token',
    ];

    protected $casts = [
        'name'              => 'string',
        'email'             => 'string',
        'gender'            => 'string',
        'national_id'       => 'string',
        'phone_number'      => 'string',
        'password'          => 'string',
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        'api_token'         => 'string',
        'device_token'      => 'string',
        'remember_token'    => 'string'
    ];
}
