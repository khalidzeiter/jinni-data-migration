<?php

namespace App\Models\Mappers;

use App\Models\Dist\Address;
use App\Models\Dist\AvailabilityHours;
use App\Models\Dist\Provider;
use App\Models\Dist\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Str;

class CleanerToUserDto
{
    private Collection $cleaners;
    private Collection $cleanersProviders;
    private Collection $cleanersAddresses;
    private Collection $cleanersAvailability;
    private Collection $cleanersRole;

    public function __construct()
    {
        $this->cleaners = collect();
        $this->cleanersProviders = collect();
        $this->cleanersAddresses = collect();
        $this->cleanersAvailability = collect();
        $this->cleanersRole = collect();
    }

    public static function make(Collection $collection)
    {
        $startId = User::query()->latest('id')->first();
        $startId = is_null($startId) ? 1 : $startId->id + 1;

        $startProviderId = Provider::query()->latest('id')->first();
        $startProviderId = is_null($startProviderId) ? 1 : $startProviderId->id + 1;

        $instance = new self();

        $instance->mapCleanersDataCollections($collection, $startId, $startProviderId);

        return $instance;
    }

    public function insertToDb()
    {
        DB::transaction(function () {
            $this->cleaners->chunk(100)->each(function ($chunk) {
                User::query()->insert($chunk->toArray());
            });

            $this->cleanersProviders->chunk(100)->each(function ($chunk) {
                Provider::query()->insert($chunk->toArray());
            });

            $this->cleanersAddresses->chunk(100)->each(function ($chunk) {
                DB::connection('data_migration.dist')->table('e_provider_coverage_area')->insert($chunk->toArray());
            });

            $this->cleanersAvailability->chunk(100)->each(function ($chunk) {
                AvailabilityHours::query()->insert($chunk->toArray());
            });

            $this->cleanersRole->chunk(100)->each(function ($chunk) {
                DB::connection('data_migration.dist')->table('model_has_roles')->insert($chunk->toArray());
            });
        });
    }

    private function mapCleanersDataCollections(Collection $collection, $startId, $startProviderId)
    {
        foreach ($collection as $item) {
            $id = $startId++;
            $providerId = $startProviderId++;

            $this->cleaners[] = [
                'id'                => $id,
                'name'              => $item->FullName,
                'email'             => $item->Email ?? "",
                'password'          => $item->Password ?? bcrypt(Str::random(60)),
                'national_id'       => $item->NationalID ?? "",
                'gender'            => ["", 'male', 'female'][$item->Gender] ?? "",
                'phone_number'      => $item->Phone ?? "",
                'phone_verified_at' => $item->Phone ? now() : null,
                'email_verified_at' => $item->Email ? now() : null,
                'created_at'        => Carbon::parse($item->CreatedOn),
                'updated_at'        => Carbon::parse($item->UpdatedOn),
            ];

            $this->mapCleanersRole($id);

            $this->mapCleanersProviders($item, $id, $providerId);
            $this->mapCleanersAddresses($item, $providerId);
            $this->mapCleanersAvailability($item, $providerId);
        }
    }

    private function mapCleanersProviders($cleaner, $cleanerId, $providerId)
    {
        $this->cleanersProviders[] = [
            'id'                 => $providerId,
            'name'               => json_encode([
                'en' => $cleaner->FullName
            ], JSON_UNESCAPED_UNICODE),
            'e_provider_type_id' => $cleaner->SalarySegmant + 1,
            'user_id'            => $cleanerId,
            'description'        => json_encode([
                'en' => $cleaner->FullName
            ], JSON_UNESCAPED_UNICODE),
            'phone_number'       => $cleaner->Phone ?? "",
            'mobile_number'      => $cleaner->Phone ?? "",
            'availability_range' => 0,
            'available'          => 0,
            'featured'           => 0,
            'accepted'           => intval($cleaner->IsActive),
            'active'             => 0,
            'rank'               => $cleaner->Rank ? ($cleaner->Rank - 1) : 0,
            'created_at'         => Carbon::parse($cleaner->CreatedOn),
            'updated_at'         => Carbon::parse($cleaner->UpdatedOn),
        ];
    }

    private function mapCleanersAddresses($cleaner, $providerId)
    {
        foreach ($cleaner->locations as $address) {
            $districtId = $address->LocationID ?? null;

            $addressArr = [
                'e_provider_id' => $providerId,
                'area_id'       => Address::getCurrentAreaId($districtId),
            ];
            if (empty($addressArr['area_id'])) continue;

            $this->cleanersAddresses[] = $addressArr;
        }
    }

    private function mapCleanersAvailability($cleaner, $providerId)
    {
        $workingHours = $cleaner->availability->Avaliability['workingDays'] ?? null;
        if (is_null($workingHours)) return;

        foreach ($workingHours as $day => $workingHour) {
            $day = strtolower(date('l', strtotime($day)));
            $availability = [
                'e_provider_id' => $providerId,
                'day'           => $day,
            ];
            if (intval($workingHour['m']) === 1) {
                $this->cleanersAvailability[] = array_merge($availability, [
                    'start_at' => '09:00',
                    'end_at'   => '11:00',
                    'daytime'  => 1,
                    'data'     => json_encode(['en' => $day . ' Morning']),
                ]);
            }
            if (intval($workingHour['n']) === 1) {
                $this->cleanersAvailability[] = array_merge($availability, [
                    'start_at' => '16:00',
                    'end_at'   => '18:00',
                    'daytime'  => 0,
                    'data'     => json_encode(['en' => $day . ' Afternoon']),
                ]);
            }
        }
    }

    private function mapCleanersRole($userId)
    {
        $this->cleanersRole[] = [
            'role_id'    => 3,
            'model_type' => "App\\Models\\User",
            'model_id'   => $userId,
        ];
    }
}
