<?php

namespace App\Models\Mappers;

use App\Models\Dist\Address;
use App\Models\Dist\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Str;

class CustomerToUserDto
{
    private Collection $customers;
    private Collection $customersAddresses;
    private Collection $customersRoles;

    public function __construct()
    {
        $this->customers = collect();
        $this->customersAddresses = collect();
        $this->customersRoles = collect();
    }

    public static function make(Collection $collection)
    {
        $startId = User::query()->latest('id')->first();
        $startId = is_null($startId) ? 1 : $startId->id + 1;

        $instance = new self();

        $instance->mapCustomersDataCollections($collection, $startId);

        return $instance;
    }

    public function insertToDb()
    {
        DB::transaction(function () {
            $this->customers->chunk(100)->each(function ($chunk) {
                User::query()->insert($chunk->toArray());
            });

            $this->customersAddresses->chunk(100)->each(function ($chunk) {
                Address::query()->insert($chunk->toArray());
            });

            $this->customersRoles->chunk(100)->each(function ($chunk) {
                DB::connection('data_migration.dist')->table('model_has_roles')->insert($chunk->toArray());
            });
        });
    }

    private function mapCustomersDataCollections(Collection $collection, $startId)
    {
        foreach ($collection as $item) {
            $id = $startId++;

            $this->customers[] = [
                'id'                => $id,
                'name'              => $item->FirstName . ' ' . $item->LastName,
                'email'             => $item->Email ?? "",
                'password'          => bcrypt(Str::random(60)),
                'national_id'       => "",
                'gender'            => ["", 'male', 'female'][$item->Gender] ?? "",
                'phone_number'      => $item->Phone ?? "",
                'phone_verified_at' => $item->Phone ? now() : null,
                'email_verified_at' => $item->Email ? now() : null,
                'created_at'        => Carbon::parse($item->CreatedOn),
                'updated_at'        => Carbon::parse($item->UpdatedOn),
            ];

            $this->mapCustomerRole($id);

            $this->mapCustomerAddresses($item, $id);
        }
    }

    private function mapCustomerRole($id)
    {
        $this->customersRoles[] = [
            'role_id'    => 4,
            'model_type' => "App\\Models\\User",
            'model_id'   => $id,
        ];
    }

    private function mapCustomerAddresses($customer, $id)
    {
        foreach ($customer->address as $address) {
            $this->customersAddresses[] = [
                'description'      => "",
                'address'          => $address->Title ?? "",
                'latitude'         => 0,
                'longitude'        => 0,
                'default'          => 0,
                'space'            => intval($address->SquareMetersArea ?? ""),
                'building_number'  => intval($address->BuildingNumber ?? ""),
                'floor_number'     => intval($address->FloorNumber ?? ""),
                'apartment_number' => intval($address->ApartmentNumber ?? ""),
                'user_id'          => $id,
                'area_id'          => Address::getCurrentAreaId($address->LocationID ?? null),
                'street'           => $address->StreetName ?? "",
                'landmark'         => substr($address->Instructions ?? "", 0, 190),
                'building_type_id' => null,
            ];
        }
    }
}
